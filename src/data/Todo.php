<?php

namespace App\data;

class Todo
{
  public int $id;
  public string $task;
  public string $desc;
  public bool $completed;
  private static int $count = 1;

  public function __construct(string $paramTask, string $paramDesc) {
    $this->completed = false;
    $this->task = $paramTask;
    $this->desc = $paramDesc;
    $this->id = self::$count;
    self::$count++;
  }

}
