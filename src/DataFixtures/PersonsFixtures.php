<?php

namespace App\DataFixtures;

use App\Entity\Actor;
use App\Entity\Director;
use Faker\Factory;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class PersonsFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Factory::create("fr_FR");

        for ($i=0; $i < 50 ; $i++) {
            $acteur = new Actor;
            $acteur->setPrenom($faker->firstName);
            $acteur->setNom($faker->lastName);
            $manager->persist($acteur);
        }

        for ($i=0; $i < 5 ; $i++) {
            $acteur = new Director;
            $acteur->setPrenom($faker->firstName);
            $acteur->setNom($faker->lastName);
            $manager->persist($acteur);
        }

        $manager->flush();
    }
}
