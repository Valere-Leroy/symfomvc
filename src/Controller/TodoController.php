<?php

namespace App\Controller;

use App\data\Todo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route("/todo")]
class TodoController extends AbstractController
{
    private array $todolist;

    #[Route('/', name: 'app.todo', methods: "GET")]
    public function index(Request $request): Response
    {
        $session = $request->getSession();
        
        if ($session->get('todolist') == null) {
            $session->set('todolist', $this->init());
        }

        return $this->render('todo/index.html.twig', [
            'controller_name' => 'TodoController',
            "todolist" => $session->get('todolist')
        ]);
    }

    #[Route('/detail/{id}', name: 'app.todo.detail', methods: "GET")]
    public function detail(int $id, Request $request): Response
    {
        $session = $request->getSession();
        $currentTodo = null;
        foreach ($session->get('todolist') as $todo) {
           if ($todo->id === $id) {
                $currentTodo = $todo;
           }
        }

        if ($currentTodo == null) {
            $this->addFlash("warning", "La todo que vous cherchez n'existe pas");
        }
        // dd($currentTodo);
        return $this->render('todo/detail.html.twig', [
            'controller_name' => 'TodoController',
            'todo' => $currentTodo,
        ]);
    }

    #[Route('/delete/{id}', name: 'app.todo.delete', methods: "GET")]
    public function delete(int $id, Request $request): Response
    {
        $session = $request->getSession();
        $currentList = $session->get('todolist');
        foreach ($currentList as $key => $todo) {
            if ($todo->id === $id) {
                unset($currentList[$key]);
           }
        }
        
        $session->set('todolist', $currentList);

        return $this->redirectToRoute('app.todo');
    }

    #[Route('/patch/{id}', name: 'app.todo.patch', methods: "GET")]
    public function patchCompleted(int $id, Request $request): Response
    {
        $session = $request->getSession();
        $currentList = $session->get('todolist');
        foreach ($currentList as $key => $todo) {
            if ($todo->id === $id) {
                $todo->completed = !$todo->completed;
           }
        }
        
        $session->set('todolist', $currentList);

        return $this->redirectToRoute('app.todo');
    }

    private function init() {
        return [
            new Todo("apprendre symfo", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit optio, enim vero quas molestiae dolor modi rem tenetur, commodi fuga, odio quod ratione eaque debitis? Eius repellendus suscipit quae ratione.
            " ),
            new Todo("créer un controller", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit optio, enim vero quas molestiae dolor modi rem tenetur, commodi fuga, odio quod ratione eaque debitis? Eius repellendus suscipit quae ratione.
            "),
            new Todo("manipuler les données", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit optio, enim vero quas molestiae dolor modi rem tenetur, commodi fuga, odio quod ratione eaque debitis? Eius repellendus suscipit quae ratione.
            ")
        ];
    }
}
